import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import {
  Switch,
  Route,
  BrowserRouter,
  Redirect
} from "react-router-dom";
import List from "./Pages/list";
import Login from "./Pages/login";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
    <Switch>
    <Route path="/" component={Login} exact/>
    <Route path="/list" component={List} />
    <Redirect to="/" />
  </Switch>
  </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
