import React, { Component } from "react";
import invitations from "../json/invitations.json";
import updateInvitations from "../json/invitations_update.json";

class List extends Component {
  state = {
    email: localStorage.getItem("email"),
    password: localStorage.getItem("password"),
    invitations: invitations.invites,
    updateInvitations: updateInvitations.invites,
    user_id: localStorage.getItem("user_id"),
  };
  componentDidMount() {
    setInterval(() => {
      this.updateData();
    }, 5000);
  }

  Logout = (e) => {
    e.preventDefault();
    localStorage.setItem("email", "");
    localStorage.setItem("password", "");
    localStorage.setItem("user_id", "");
    window.location.href = "/login";
  };
  updateData = () => {
    const { updateInvitations, user_id, invitations } = this.state;
    if (updateInvitations.length > 0) {
      const updateData = updateInvitations.filter((d) => d.user_id === user_id);
      const oneData = updateData.pop();
      invitations.push(oneData);
      this.setState({ updateInvitations: updateData });
      console.log("invite", invitations);
    }
    console.log(updateInvitations);
  };
  render() {
    const { email, password, invitations, user_id } = this.state;
    const data = invitations.filter((d) => d.user_id === user_id);

    return email !== "" && password !== "" ? (
      <div className="container">
        <div className="row">
          <div className="col-10 align-self-start"></div>
          <button
            type="button"
            className="btn btn-primary mt-5 col-2 align-self-end"
            onClick={(e) => this.Logout(e)}
            style={{}}
          >
            Logout
          </button>
        </div>
        {data.map((data) => {
          return (
            <div className="card m-4" key={data.invite_id}>
              <div
                className={
                  data.status === "read"
                    ? "card-header bg-success text-light"
                    : "card-header bg-danger text-light"
                }
              >
                {data.vector}
              </div>
              <div className="card-body">
                <blockquote className="blockquote mb-0">
                  <p>{data.invite}</p>
                </blockquote>
              </div>
            </div>
          );
        })}
      </div>
    ) : (
      <div className="container">
        <div className="alert alert-danger mt-4" role="alert">
          You're Not Authorized!!
        </div>
        <div className="text-primary">
          First, You have to Login! <a href="/login">Login Here</a>
        </div>
      </div>
    );
  }
}

export default List;
