import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import users from "../json/users.json";

var bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10);

class Login extends Component {
  state = {
    users: users,
    email: "",
    password: "",
    local_email: localStorage.getItem("email"),
    local_password: localStorage.getItem("password"),
  };
  componentDidMount() {
  }
  handlechange = (e) => {
    e.persist();
    this.setState({ [e.target.name]: e.target.value });
    console.log(this.state.password);
  };
  handleSubmit = (e) => {
    e.preventDefault();
    if (this.state.users.users.map((u) => u.email).includes(this.state.email)) {
      if (
        this.state.users.users
          .map((us) => us.password)
          .includes(this.state.password)
      ) {
        window.location.href = "/list";
        localStorage.setItem('email',this.state.email);
        localStorage.setItem('password',bcrypt.hashSync(this.state.password, salt));
        localStorage.setItem('user_id',this.state.users.users.filter(e => e.email === this.state.email).map(r => r.user_id).toString())
      }
      else{
        window.location.href = "/";
      }
    }else{
      window.location.href = "/";
    }
  };
  render() {
    const { local_email, local_password } = this.state;
    return (
      local_email === "" && local_password === "" ?
      <>
        <div className="container mt-5">
          <form className="row g-3">
            <div className="col-md-6">
              <label htmlFor="email" className="form-label">
                Email
              </label>
              <input
                type="email"
                className="form-control"
                id="email"
                name="email"
                onChange={(e) => this.handlechange(e)}
                required
              />
            </div>
            <div className="col-md-6">
              <label htmlFor="password" className="form-label">
                Password
              </label>
              <input
                type="password"
                className="form-control"
                id="password"
                placeholder="Password"
                name="password"
                onChange={(e) => this.handlechange(e)}
                required
              />
            </div>
            <div className="col-md-6">
              <button
                type="submit"
                className="btn btn-primary mb-3"
                onClick={(e) => this.handleSubmit(e)}
              >
                Login
              </button>
            </div>
          </form>
        </div>
      </> : <Redirect to="/list"/>
    );
  }
}

export default Login;
